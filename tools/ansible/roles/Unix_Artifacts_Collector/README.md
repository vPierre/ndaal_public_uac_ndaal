# Ansible Role for Unix-like Artifacts Collector (UAC)

This ansible role is a part of *Linux_Forensic_Information* series. The series aims to collect set of logs/information of linux hosts using ansible roles that are executed as part of an ansible playbook [Linux_Forensic_Information.yml](./../../../Linux_Forensic_Information.yml). 

UAC is a Live Response collection script for Incident Response that makes use of native binaries and tools to automate the collection of AIX, Android, ESXi, FreeBSD, Linux, macOS, NetBSD, NetScaler, OpenBSD and Solaris systems artifacts. 

As part of the series, this ansible role uses the [Unix-like Artifacts Collector](https://github.com/tclahr/uac) tool to collect full artifacts of the system, and generate archived logs that are fetched onto the ansible controller. Since the tool does not require installation, rather a repository clone, it is removed during the clean up stage after the logs have been collected.

The role produces and collects two files:
1. Compressed logs in the format uac-<ip_address>-linux-<timestamp>.tar.gz
2. Log file containing the name and the hash of the compressed logs in the format uac--<ip_address>-linux-<timestamp>.log

## Requirements

This role has no special requirements. However, the tool requires elevated privileges so it needs to be run as root or a sudo user.

## Role Variables

This role has two variables that can be modified [here](./vars/main.yml):

- The `uac_dir` refers to the temporary directory where the UAC is downloaded and executed. logs will be collected before and after archive.
- The `results_dir` refers to the temporary directory inside the `uac_dir` where the results will be collected. 

During the cleanup process, both the above directories will also be removed to cleanup space on the system.

```yaml
uac_dir: "/home/{{ ansible_user }}/uac"
results_dir: "/home/{{ ansible_user }}/uac/uac_results"
```
The default variables of the role refer to the UAC version that will be used to collect artifacts.
It can be modified [here](./defaults/main.yml)

```yaml
uac_version: "3.0.0"
uac_src_url: "https://github.com/tclahr/uac/releases/download/v{{ uac_version }}/uac-{{ uac_version }}.tar.gz"
```

## Example Playbook:

```yaml
---
- hosts: linux
  gather_facts: true
  become: true
  become_method: sudo
  vars:
    local_path: "/home/admin"
  pre_tasks:
    - name: Set filename including the IP address of the host
      ansible.builtin.set_fact:
        logfile_prefix: "logs"
        logfile_suffix: "{{ ansible_hostname }}_{{ ansible_default_ipv4.address }}_{{ ansible_fqdn }}_{{ ansible_date_time.iso8601_basic_short }}"
    - name: Set foldername including the timestamp for all archives
      ansible.builtin.set_fact:
        log_dir: "{{ local_path }}/logs_{{ ansible_date_time.iso8601_basic_short }}"
    - name: Ensure {{ log_dir }} exists on ansible controller
      ansible.builtin.file:
        path: "{{ log_dir }}"
        state: directory
        mode: '0755'
        owner: "{{ ansible_user }}"
        group: "{{ ansible_user }}"
      delegate_to: localhost
  roles:
    - role: Linux_Forensic_Information/Unix_Artifacts_Collector
```

## References

- [Unix-like Artifacts Collector](https://github.com/tclahr/uac)

## Contribution

Contributions are welcome! Please submit pull requests or open issues.

## License

Apache 2.0.

## Authors

- Pierre Gronau ([Pierre.Gronau@ndaal.eu](mailto:Pierre.Gronau@ndaal.eu))
- Ayesha Shafqat ([ayesha.shafqat@ndaal.eu](mailto:ayesha.shafqat@ndaal.eu))

**Organization**: ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG